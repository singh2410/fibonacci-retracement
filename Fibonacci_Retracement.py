#!/usr/bin/env python
# coding: utf-8

# # Fibonacci Retracement for Upward trend
# #By- Aarush Kumar
# #Date: May 17,2021

# In[2]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')


# In[3]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Fibonacci Retracement/data.csv')
df=df.set_index(pd.DatetimeIndex(df['Date'].values))


# In[4]:


df


# In[5]:


#plotting close price on chart
plt.figure(figsize=(13,5))
plt.plot(df.Close, color = 'blue')
plt.title('S&P 500 Close Price')
plt.xlabel('Date')
plt.ylabel('Price(USD)')
plt.show()


# In[6]:


maximum_price=df['Close'].max()
minimum_price=df['Close'].min()
difference=maximum_price-minimum_price
first_level=maximum_price-difference*0.236
second_level=maximum_price-difference*0.382
third_level=maximum_price-difference*0.5
fourth_level=maximum_price-difference*0.618


# In[7]:


print('Level Percentage Price ($)')
print('0.00%\t\t',maximum_price)
print('23.6%\t\t',first_level)
print('38.2%\t\t',second_level)
print('50.0%\t\t',third_level)
print('61.8%\t\t',fourth_level)
print('100.0%\t\t',minimum_price)


# In[8]:


new_df=df
plt.figure(figsize=(13,5))
plt.title('Fibonacci Retracement Plot')
plt.plot(new_df.index, new_df['Close'])
plt.axhline(maximum_price,linestyle='--',alpha=0.5,color='red')
plt.axhline(first_level,linestyle='--',alpha=0.5,color='orange')
plt.axhline(second_level,linestyle='--',alpha=0.5,color='yellow')
plt.axhline(third_level,linestyle='--',alpha=0.5,color='green')
plt.axhline(fourth_level,linestyle='--',alpha=0.5,color='blue')
plt.axhline(minimum_price,linestyle='--',alpha=0.5,color='purple')
plt.xlabel('Date')
plt.ylabel('Price')
plt.show()


# In[9]:


new_df=df
fig=plt.figure(figsize=(13,5))
ax=fig.add_subplot(1,1,1)
plt.title('Fibonacci Retracement Plot')
plt.plot(new_df.index, new_df['Close'],color='black')
plt.axhline(maximum_price,linestyle='--',alpha=0.5,color='red')
ax.fill_between(new_df.index,maximum_price, first_level, color='red')

plt.axhline(first_level,linestyle='--',alpha=0.5,color='orange')
ax.fill_between(new_df.index,first_level,second_level, color='orange')

plt.axhline(second_level,linestyle='--',alpha=0.5,color='yellow')
ax.fill_between(new_df.index,second_level,third_level, color='yellow')

plt.axhline(third_level,linestyle='--',alpha=0.5,color='green')
ax.fill_between(new_df.index,third_level, fourth_level, color='green')

plt.axhline(fourth_level,linestyle='--',alpha=0.5,color='blue')
ax.fill_between(new_df.index,fourth_level,minimum_price, color='blue')

plt.axhline(minimum_price,linestyle='--',alpha=0.5,color='purple')

plt.xlabel('Date')
plt.ylabel('Price')
plt.show()

